# builder-boilerplate

# Dependencies
- ``nodejs``
- ``npm``

# Usage
##### 1. Clone or download project to target folder
##### 2. run `npm install` or `npm i` for install packages 
##### 3.0 get or create global config
```javascript
const globalConfig = {
	config: {
    		formId: "formId", // form id
    		name: "text_area_name", // name for textarea `store`
    	},
    	items: [
    		{
    			id:"item_rtb_lXCIKjP",
    			key:"raw",
    			fields:{
    				title:"",
    				appearance:{
    					fill:"clear",
    					size:"small"
    				}
    			}
    		}
    	],
    	buttons: [
    		{"key":"raw","title":"Ricje","icon":"TextAlignFull"},
    		{"key":"image-text","title":"Image Text","icon":"TextWrap"},
    	]
}

// and pass it to `initBuilder` method
function initBuilder(config) {
 // ... do something cool ...     
}
initBuilder(globalConfig);

```

##### 3.1 Add fields to ``source/fields`` folder and update mapping in ``source/fields/index.js``
```javascript
// ...more imports
import NewSuperField from "path/to/NewSuperField";

const externalFields = {
	"TemplateKey": TemplateField, // Template
	"NewSuperKey": NewSuperField
};    
```  
#### 4. Import missed icons from ``grommet-icons`` and update mapping in ``source/icons/index.js``
```javascript
// ...more imports
import SuperIcon from "grommet/icons/SuperIcon";

const externalIcons = {
	SuperIcon
};    
```  
#### 5. Update ``deserialize.js`` and ``serialize.js``
- deserialize - transform data from store to editor state
- serialize - transform data from editor state to store
##### 5.1. Or use EditorSchema if methods are exist in ``builder-utils``
 
 

# Structure of directories
- ``source/builder`` - includes externals fields, icons and utils and init file ``builder.js``
- ``source/style`` - scss source of editor.global.css 
- ``assets`` - dist folder for builder for transpiled js/css and static files (images, icons, eg)

# NPM 
- ``npm run dev`` - run webpack in dev mode
- ``npm run prod`` - run webpack in prod mode
- ``npm run style:dev`` - run in dev mode a node-sass for compile ``source/master.scss`` to ``assets/editor.global.css`` 
- ``npm run style:prod`` - run in prod mode a node-sass for compile ``source/master.scss`` to ``assets/editor.global.css``
- ``npm run build`` - sequentially run ``npm run prod`` and ``npm run style:prod``   
- ``npm run config:generate`` - generate config from from `./config/index.js` to `assets/editor.config.json`

# Config generator
coming soon ... 

# Підключення білдера на сторінку
1. Підключити `builder.js` до цільової сторінки
2. На сторінку додати елемент з ідентифікатором (селектор). Елемент або селектор передати як показано нижче
```javascript
const { render } = configure({
    config: {deserialize, serialize},
    icons: externalIcons,
    fields: externalFields,
    root: "#block-builder-draft-root" // or HTMLElement
});
render(() => {
    console.log("App was rendered");
});
```


## Builder 3.3.3 (Nov 13, 2019)
Add events and tips

### Events `onItemDelete` and `onBlockDelete` as listen events. Both return deleted item's data.
More about event listeners in section **3.3.0 (Oct 21, 2019)**

```js
	rootNode.addEventListener("onItemDelete", (e) => {
		console.group("Remove ITEM (aka Widget)");
		console.log(e.detail);
		console.groupEnd()
	});
	
	rootNode.addEventListener("onBlockDelete", (e) => {
		console.group("Remove BLOCK (array's item)");
		console.log(e.detail);
		console.groupEnd()
	});
```

### Tips for fields

#### Preview:
![Default preview of tips](/media/tips1.png)

#### Features:

1. Custom styles for content wrapper. Assing to className `StyledFieldTips`
2. Code level
 `<TipsContentWrapper className="StyledFieldTips">{# ...items #}</TipsContentWrapper>`
    
3. Visual level
![Default preview of tips](/media/tips2.png)

4. Pass any valid html/React/svg content to tips
5. Implement your prefer way to get content data: static, API, JSON.

#### Steps to create
[1] Create tips item
```js
const SomeTip = () => (
	<p>Some external tip. <br /> With <code>br/</code></p>
);
```
It's very important!!!
- item must be a valid React component. It will check through `React.isValidElement`.
In falsy cases will rendered `null`
- wrapper of item has `pointe-events: none`

[2] Add the item to mapping
```js
const externalTips = {
	"SomeTip": SomeTip
};

export default externalTips;
```

[3] Pass mapping builder's `configure` method to `config` property
```js
    const { render, getState, setState, rootNode } = await configure({
        // other `configure` arguments
        // icons: externalIcons,
        // fields: externalFields,
        // views: externalViews,
        tips: externalTips
    });
```


[4] Assign tips item key to field in config. 
Pass t model -> a certain field -> field options -> `tipsKeys` property.

`tipsKeys :: string[]` 

```js
	model("quote", "Цитата з автором", {
		
		fieldsAppearance: { 
		  // properties
        },
		fields: [
			multilineString("text", "Текст цитати", {
				validate: { key: "minLength", config: 20, description: "Мін 20 символів" },
				limits: { maxLength: 100 },
				options: {
                    // HERE IS
					tipsKeys: ["SomeTip"]
					// tipsKeys: ["SomeTip", "SomeTip2", "SomeTipN-key]
				}
			}),
		]
	})
```    

### Migrate to 3.3.3
update packages: 
- @aurocraft/builder to 3.3.3
   
actions:
- [optional] add listeners for `onItemDelete` and `onBlockDelete`
- [optional] create and use tips for field. Follow steps above


## Builder Config Generator up to 4.0.0 - Appearances
Reinvent way to set appearance for model

**Define appearance** for model via prop `apperance`. `apperance` is [] or "*"

    ```js
     model("quote", "Цитата з автором", {
        appearance: ["*", {
            "size": "small"
        }],
        // ... something else
     })
    ```

Allowed values

- `"*"` - all of predefined (DEFAULT value)

- `[ ["*", {} ] ]` - all of predefined with values

- `["key1", "key2"]` - for select some set

- `[ ["key1", "non-default-value"], "key2"]` - for select some set with overwrite value
 
- `[ createAppearance(config) ]` - for create custom. Where config is { key, name, value, options: {} }

- or combine
```json
    [
      ["key1", "non-default-value"],
      "key2",
      createAppearance({
        key: "customKey",
        name: "customName",
        value: "val1",
        options: {
          "val1": "val1",
          "val2": "val2"
        }
      })
    ]
```
```json
    [
      ["*", {} ],
      createAppearance({
        key: "customKey",
        name: "customName",
        value: "val1",
        options: {
          "val1": "val1",
          "val2": "val2"
        }
      })
    ]
```


**Examples of usages**

1 - All from predefined config with default value for each
```js
model("quote", "Цитата з автором", {
    appearance: ["*"],
    // ... something else
 })
```
2 - All from predefined config with custom value for something
```js
model("quote", "Цитата з автором", {
        appearance: [
     	  ["*", {
            "size": "large", 
           // where `size` is key of predefined appearance item and `large` is one of the valid property 
         }]
       ],
        // ... something else
     })
```
3 - All from predefined config and 1 custom appearance
```js
model("quote", "Цитата з автором", {
    appearance: [
      ["*", {}], // or short ["*"] or ["*", { "size": "large", }] for set value
      createAppearance({
       key: "test",
       name: "name",
       value: "option_2",
       options: {
          "option_1": "First option",
          "option_2": "Second option",
       }
      })
   ],
    // ... something else
 })
```
4 - Some from predefined
```js
model("quote", "Цитата з автором", {
    appearance: [
      "fill" // or multiple: "fill", "size"
    ],
    // ... something else
 })
```
    
5 - Some from predefined with custom value
```js
model("quote", "Цитата з автором", {
    appearance: [
      ["fill", "grey"]
    ],
    // ... something else
 })
```

6 - Some from predefined and 1 custom appearance
```js
     model("quote", "Цитата з автором", {
        appearance: [
          "fill", // ["fill", "grey"]
          createAppearance({
             key: "test",
             name: "name",
             value: "option_2",
             options: {
              "option_1": "First option",
              "option_2": "Second option",
             }
            })
        ],
        // ... something else
     })
```    
7 - Just 1 custom
```js
 model("quote", "Цитата з автором", {
    appearance: [
      createAppearance({
         key: "test",
         name: "name",
         value: "option_2",
         options: {
          "option_1": "First option",
          "option_2": "Second option",
         }
        })
    ],
    // ... something else
 })
```
8 - None
```js
     model("quote", "Цитата з автором", {
        appearance: null,
        // ... something else
     })
```       

### Migrate to 4.0.0
update packages: 
- @aurocraft/builder-config-generator to 4.0.0
   
actions:
- redefine `apperance` options according to above guides
- regenerate json config


## 3.3.1 (Oct 21, 2019)
Fix preview for non-exist key

## 3.3.0 (Oct 21, 2019)

#### Listen updating via CustomEvent

Pass to `config` in `configure` method  

List of events
 - onInit - fire one time when builder was mount and initial state was set 
 - onForceUpdated - after setState call
 - onUpdate - state updated
 - onChange - store updated
 - "*" - represent all above
 
**Usage**
```js
// Define which events will be listen
const { render, getState, setState, rootNode } = await configure({
    config: {
        // ... other `config` options
        listenEvents: "*", 
        // listenEvents: ["onInit", "onChange", "onUpdate", "onForceUpdated"]
        // listenEvents: ["onChange"]
    },
    // other `configure` arguments
});

// Then add handlers for each event
rootNode.addEventListener("onUpdate", (e) => {
    console.log("onUpdate", e.detail)
    // e.detail === getState()
});
// ... other event listeners
```

#### Option `arrayItemsCollapsed` - for collapse array items.
Pass to `config` in `configure` method
If `true` - array items will collapsed every time. State won'n save.

```js
const { render, getState, setState, rootNode } = await configure({
    config: {
        // ... other `config` options
        arrayItemsCollapsed: true,
    },
    // other `configure` arguments
});
   
```
#### Views for Sort Mode
!!! Attention. Might be break.
Past `fieldsAppearance.previewKey` was used for output some field value. It was changed. Instead key of field 
`previewKey` **now represent key of view in views mapping**. 

Here use the same approach as with fields/icons/validations
Steps
0) Create view React component 
    ```js
     import React from "react";
     
     const QuoteView = ({item}) => {  	
     // item - is single item. 
     // const { key, id, title, fieldsAppearance, fields } = item;
     	
     	return (
     		<div>
     			{ item.fields.text } <br />
     			<b>{ item.fields.author }</b>
     		</div>
     	);
     };
     
     export default QuoteView;
    ```
1) In project: Define views mapping
    ```js
        import QuoteView from "./QuoteView";

        const externalViews = {
     	    // [key: string]: ReactNode
        	"QuoteView": QuoteView
        };
        
        export { externalViews };
    ```

2) In builder `configure` method - pass externals views mapping to key `view`
    ```js
        const { render, getState, setState, rootNode } = await configure({
            // other `configure` arguments
            // icons: externalIcons,
            // fields: externalFields,
            views: externalViews,
        });
    ```
3) In config. For model assign view key to `fieldsAppearance.previewKey`
    ```js
     model("quote", "Цитата з автором", {
        fieldsAppearance: {
            grid: null,
            previewImage: null,
             // multi view
            previewKey: ["ItemIdView", "QuoteView"],
            // or single view
            // previewKey: "ItemIdView",
        },
        fields: [
           // fields
        ]
     })
    ```

#### Fix
- Fix select options order for numeric value
- Fix missed `max-length` for `MultilineString` (textarea). Might passed via `limits` prop of field
- Fix validation issue for array item with depth more than 2


### Migrate to 3.3.0
update packages: 
- @aurocraft/builder to 3.3.0
   
actions:
- [optional] add `config.listenEvents` and then listen via `rooNode.addEventListener([name], e => {})`

- [optional] define `arrayItemsCollapsed`. default value is `false`

- [optional] pass views mapping to configure  


## 3.2.0 (Oct 11, 2019)
Add methods `getState` and `setState` for builder instance. And return builder `rootNode`;
These 2 methods and rootNode return from `configure` builder additional to `render` method


**!!! Important**
Use `getState` and `setState` only after builder instance was configured and mounded 

```js
const { render, getState, setState, rootNode } = await configure({
    // ...configure options
})
```

### Usage example
##### 1. Custom button factory

```js
const createButton = (title, event, builderNode) => {
	const button = document.createElement("div");
	button.className = "btn btn-info";
	button.textContent = title;
	button.addEventListener("click", event);
	builderNode.parentNode.insertBefore(button, builderNode);
	return button;
};
```
##### 2. Define `FORCE_DATA` (or get in something else way)
```js
const FORCE_STATE = [
	{
		"key": "quote",
		"id": "item_quote__WO0sOf",
		"fields": {
			"text": "force TEXT",
			"author": "force AUTHOR",
			"label": "force LABEL",
			"appearance": {
				"size": "large",
				"fill": "clear"
			}
		}
	},
	{
		"key": "raw",
		"id": "item_raw_IzdMyRd",
		"fields": {
			"text": "<p>force RAW PARAGRAPH</p>",
			"appearance": {
				"size": "large",
				"fill": "clear"
			}
		}
	}
];
```

##### 3. In builder's instance method `render` crete to buttons with listen to click
```js

render(() => {
		
		createButton(
			"GET Builder State",
			() => console.log(JSON.stringify(getState(), null, 4)),
			rootNode
		);
		
		createButton(
			"SET Builder State",
			() => setState(FORCE_STATE),
			rootNode
		);
		
		
		console.log("App was rendered. Use package. v3"
		);
	});

```

### Migrate to 3.2.0
update packages: 
- @aurocraft/builder to 3.2.0   
actions:
- optional



## 3.1.1 (Sep 25, 2019)
Add two methods for transform data

```js

const { render } = await configure({
		config: {
			deserialize,
			serialize,
			/**
			 * Run before deserialization
			 * @param data - stored data.
			 */
			beforeEncode: function(data) {
				return data
			},
			/**
			 * Run after store all fields to storage
			 * @param data - serialized data from Storage
			 */
			afterDecode: function (data) {
				return data;
			},
			globalConfig,
			editorConfig: "/assets/editor.config.json"
		},
		icons: externalIcons,
		fields: externalFields,
		validations: externalValidations,
		root: "#block-builder-draft-root"
	});
	render(() => {
		console.log("App was rendered");
	});

```
### Migrate to 3.1.1
update packages: 
- @aurocraft/builder to 3.1.1   
actions:
- optional


## 3.0.0 (Sep 25, 2019)
### !!!BREAKING CHANGES ###
**NEW de-/serialization way**
+ deserialization: mechanism of creating and adding items, blocks and fields integrated into @aurocraft/builder in method `decode`
+ serialization: mechanism of finding blocks and extract them data and map of items integrated into @aurocraft/builder in method `encode`

    
### Migrate to 3.0.0
update packages: 
- @aurocraft/builder-config-generator to 3.0.0
- @aurocraft/builder to 3.0.1
- REMOVE @aurocraft/builder-libs. All useful parts integrated into @aurocraft/builder   
actions:
- rewrite your `deserializator` and `serializator` method according to new guides. 
You probably find usage examples and some jsDoc explains in folder `/source/main/utils/shell-examples`


## 2.4.0 (Sep 23, 2019)

### !!!BREAKING CHANGES ###
- Now EntityItem is a part of @aurocraft/builder package.
- now method `createItem` must be export from @aurocraft/builder package
- prepare to remove `onValidate` as props of Field. Now validation is fire every onChange event and update validation state automatically.

### add default classNames for field and block  ###
- now field has
  - default css-class `c-builder__field` 
  - custom css-class `"CustomField--" + [field].options.className`
- now block/widget has
  - default css-class `c-builder__block`
  - custom css-class `"CustomBlock--" + [model].fieldsAppearance.className`

### fix and under the hood changes ###
- fix validation for Array-type. In some cases, data may not be saved
- move validation mechanism from view to store
- move visibility mechanism from view to store
- add dynamic property `parent` for block. It's reference to parent Item for Array items. For elements of `items`,  `parent` property is `null`  
- something else small fixing and changes


### Migrate to 2.4.0
update packages: 
- @aurocraft/builder to 2.4.0
actions:
- in `deserialize` method `createItem` from `@aurocraft/builder` instead `@aurocraft/builder-libs`


## 2.2.3 (Sep 18, 2019)
- fix z-index bug options tooltip
- pass `endpoint` to IconField via options
Now, list of options used inside IconField looks like:
```
options: {
    endpoint: "/api/v1/custom/path/to/load/icon", // NEW
    accept: [".png", ".svg"],
    storagePathPrefix: "/storage/"
},
```

Reflect in IconField
```js
  const requestUrl = this.props.options.endpoint || "/api/v1/upload/icon";
```

### Migrate to 2.2.3
update packages: 
- @aurocraft/builder to 2.2.3
actions
- optional add endpoint to field with type Icon

## 2.2.2 (Sep 16, 2019)
fix recursive array type

### Migrate to 2.2.2
update packages: 
- @aurocraft/builder to 2.2.2
actions
- make sure your de-/serializer ready to work with recursive fields 

## 2.2.0-1 (Sep 16, 2019)
### Custom className for field via field.options.className
Custom className is actually className postfix, because in result we get 
```js
const customClassName = "CustomField--" + className
// for example CustomField--textarea, where `textarea` passed via config
```

`className` will added to FieldWrapper in SingleField and will include label and Field

In config add:
```
{
  ... rest model props
  "fields": [{
    ...rest field props
    "options": {
        "className": "textarea"
      },
  }]
}
```


### Custom className for block via fieldsAppearance.className
Custom className is actually className postfix, because in result we get 
```js
const customClassName = "CustomBlock--" + className
// for example CustomBlock--quote, where `quote` passed via config
```

`className` will added to ItemFormContainer and will include all fields
 
In config add:
```
{
  "key": "quote",
  "title": "Цитата з автором",
  "view": "Debug",
  "fieldsAppearance": {
    "className": "quote", // ClassName postfix
    "grid": [
      [ "text" ],
      [ "author", "label" ]
    ],
    "previewKey": "text"
  },
  "fields": [...]
}
```

### Migrate to 2.2.0
update packages: 
- @aurocraft/builder to 2.2.1
- @aurocraft/builder-elements to 1.0.6
actions:
- update models config for project
- npm run config:generate
- add style for editor.global.css



## 2.1.0 (Sep 16, 2019)

### Visibility of field
by default all fields are visible. For change visibility of field, now we can use property `visibility`. 
There are 3 kind of options
1. Static
```
    visibility: true // default value
    // or 
    visibility: false
```
set `false` for simple hide field. In output will pass default value, defined in config 

2. Behavioral object  
```
    visibility: {
        valuePath: "model.target",
        test: "_self",
        action: "hide"
    }
```
`valuePath` - path to property, where `model` represents current block/model
`test` - value that will compare with extracted value by `valuePath`,
`action` - `hide` or `show`
3. Behavioral array 
```
	visibility: [
		{
			valuePath: "model.target",
			test: "_self",
			action: "hide"
		},
		{
			valuePath: "model.target",
			test: "_blank",
			action: "show"
		}
	]
``` 
the same as in p.2, but we can use more than 1 test.
!!! **BE CAREFUL** in array mode checks process in order from first to last and return first truth result

### Migrate to 2.1.0
update packages: 
- @aurocraft/builder-config-generator to 2.1.0
actions:
- update models config for project
- npm run config:generate


## 2.0.x (Sep 10, 2019)

- "@aurocraft/builder-config-generator": "^2.0.0",
- "@aurocraft/builder-libs": "^2.0.0",
- "@aurocraft/builder": "^2.0.1",

### !!!Breaking changes
1) `field.validations` change to `field.limits`. Now **defaultField**  has schema as shown below:
```javascript
    const defaultField = {
    	key: null,
    	type: null,
    	name: null,
    	value: "",
    	options: {},
    	itemsIds: [],
    	limits: {}, // changed from `validations`
    	validate: { 
    		key: "none",
    		config: "",
    		description: ""
    	}, // brand new!
    	items: {
    		modelKey: null,
    		options: null,
    		limits: null
    	}
    };
```
2) async `initBuilder` and `configure`
3) invalid block **can't** store to `textarea`;

### pass new method onValidate to field
Now field has 5 props: 
    - onChange - *required
    - value - *required
    - onValidate
    - limits // old name `validations`
    - options

### Validate object

`validate.key` - func. name of function. It gets `value` as argument and must return Boolean
`validate.config` - any. It will be pass as argument for first exec
`validate.description` - string. Show as a tip below field

Example
```javascript
const validateItemsCount = (config) => (value) => {
	const { min, max } = config;
	
	return value.length >= min && value.length <= max;
};
export { validateItemsCount };

```

### Validations mapping
Added to boilerplate

Steps
1. create folder with name `validations`
2. inside `validations` create file `index.js`
3. inside `validations` create files for every methods. for example: `isRequired.js`
4. create custom validation function `isRequired` inside `validations/isRequired.js`. for example
    ```js 
     const isRequired = (config) => value => {
     	return !!value
     };
     
     // config - any. will passed from editor.config.js
     // value - any. will passed from editor state
     // @return Boolean 
     
     export { isRequired };
     ```
5. in `validations/index.js` create mapping. 
    ```javascript
     import { isRequired } from "./isRequired"; // 1) import recently created function
   
     const externalValidations = { // 2) create some object and map all functions
     	isRequired, // or `isRequired: isRequired` in es5 style
     };
     
     export { externalValidations }; // export your mapping
    ```
6. in main file `builder.js`
    - import new mapping `import { externalValidations } from "./main/validations";`
    - pass to builder `configure` method
       ```javascript
          const { render } = await configure({
          		config: {deserialize, serialize, globalConfig, editorConfig: "/assets/editor.config.json"},
          		icons: externalIcons,
          		fields: externalFields,
          		validations: externalValidations, // !!!HERE
          		root: "#block-builder-draft-root"
          	});
       ```
7. for some field use `isRequired` in config, like below
    ```javascript
     //...
     validate: {
	    key: "isRequired",
        config: null,
        description: "Це поле обовязкове."
     }
     //...
    ```
    
### Block
-  add property `isValid` 
-  add property `validate` list of messages every fields

## 1.2.1 (Sep 6, 2019)
- pass `validations` to `SingleField`. Now field has 4 props: 
    - onChange
    - value
    - validations
    - options
- `maxLength` for `InputField`
- rename titles for dev tabs and change they order: 
    - Debug [dev] -> [editor state]
    - Output [dev] -> [to store]
    - APP_STORE [dev] -> [from store]

const configPreferences = require("./source/preferences");
const configModels = require("./source/models");
const configAppearances = require("./source/appearances");
const { ConfigGenerator } = require("@aurocraft/builder-config-generator"); // Relative path to module

ConfigGenerator({
	data: {
		models: configModels,
		preferences: configPreferences,
		appearances: configAppearances
	},
	outputPath: "./assets/editor.config.json"
});

const configPreferences = {
	globalCss: "/assets/blocks-builder/assets/css/editor.global.css",
	startRecommendation: 3,
	assetsRoot: "/assets/blocks-builder/assets/", // trailing slash is required
	askBeforeRemove: true
};

module.exports = configPreferences;

const configAppearances = [
	{
		"key": "size",
		"type": "Tabs",
		"name": "Ширина блоку",
		"value": "large",
		"options": {
			"small": "Вузький",
			"large": "Звичайний",
			"full": "На всю ширину"
		}
	},
	{
		"key": "fill",
		"type": "Tabs",
		"name": "Стиль фону",
		"value": "clear",
		"options": {
			"clear": "Звичаний",
			"grey": "Виділений"
		}
	}
];

module.exports = configAppearances;

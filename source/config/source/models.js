const {
	model,
	options: { imageOptions, selectOptions },
	field: {
		richText,
		boolean,
		image,
		icon,
		multilineString,
		select,
		string,
		custom,
		array
	}
} = require("@aurocraft/builder-config-generator");

const useModel = (key, props = {}) => {
	return Object.assign({}, { modelKey: key }, props);
};

const createLinkItemField = (key = "button", title = "Кнопка-Ссилка") => {
	return array(key, title, {
		limits: { maxCount: 1 },
		items: useModel("link-item")
	})
};

const configModels = [
	model("raw", "Форматований текст (Rich Editor)", {
		view: "RichText",
		fields: [
			richText("text", "Форматований текст"),
		]
	}),
	model("quote", "Цитата з автором", {
		fieldsAppearance: {
			grid: [
				["text"],
				["author", "label"]
			],
			previewKey: ["ItemIdView", "QuoteView"],
			previewImage: "media/preview-1.jpg"
		},
		fields: [
			multilineString("text", "Текст цитати", {
				validate: { key: "minLength", config: 20, description: "Мін 20 символів" },
				limits: { maxLength: 100 }
			}),
			string("author", "Автор цитати",{
				validate: { key: "minLength", config: 2, description: "Мін 2 символи" },
				limits: { maxLength: 100 }
			}),
			string("label", "Опис автора - посада, позиція і тд", {
				limits: { maxLength: 100 }
			}),
		]
	}),
	model("video", "Youtube відео", {
		fieldsAppearance: {
			grid: [
				["youtubeId", "caption"],
				["placeholder"]
			],
			previewKey: "caption"
		},
		fields: [
			string("youtubeId", "Youtube video Id"),
			string("caption", "Підпис для відео"),
			image("placeholder", "Постер для відео", {
				value: {},
				options: imageOptions({
					cropOptions: ["9/16"],
					accept: [".jpg", ".jpeg", ".png"]
				})
			}),
		]
	}),
	model("image", "Зображення", {
		fields: [
			image("media", "Зображення", {
				value: {},
				options: imageOptions({
					cropOptions: [
						"9/16", // 16:9
						"3/4",  // 4:3
						"1/2"   // 2:1
					],
					accept: [".jpg", ".jpeg", ".png"]
				})
			})
		]
	}),
	model("gallery", "Галерея зображень", {
		fields: [
			array("items", "Галерея зображень", {
				items: useModel("image")
			})
		]
	}),
	model("image-text", "Зображення з форматованим текстом", {
		fieldsAppearance: {
			previewKey: "text"
		},
		fields: [
			richText("text", "Текст"),
			image("media", "Зображення", {
				value: {},
				options: imageOptions({
					cropOptions: ["9/16"],
					accept: [".jpg", ".jpeg", ".png"]
				})
			}),
			select("orientation", "Порядок виведення блоків", {
				value: "default",
				options: selectOptions({
					defaultLabel: "Оберіть один з варіантів",
					items: {
						"default": "Звичайний: фото | текст",
						"reverse": "Зворотній: текст | фото"
					},
				})
			})
		]
	}),
	model("rtb-item", null, {
		fieldsAppearance: {
			grid: [
				["text", "icon"]
			]
		},
		fields: [
			icon("icon", "Іконка", {
				"options": {
					"accept": [".png", ".svg"],
					"storagePathPrefix": "/storage/"
				}
			}),
			string("text", "Заголовок"),
		]
	}),
	model("stats-item", null, {
		fieldsAppearance: {
			grid: [
				["stats", "title"],
				["description"]
			]
		},
		fields: [
			string("stats", "Значення статистики", {
				limits: { maxLength: 6 },
			}),
			string("title", "Заголовок"),
			multilineString("description", "Опис")
		]
	}),
	model("link-item", null, {
		fieldsAppearance: {
			grid: [
				["link","caption","target"]
			]
		},
		fields: [
			string("caption", "Заголовок"),
			string("link", "Урл", {
				validate: {
					key: "isLink",
					config: {},
					description: "Введіть урл правильного формату. Наприклад: https://sitename.com.ua/path/to/page"
				}
			}),
			select("target", "Відкривати", {
				value: "_self",
				options: selectOptions({
					defaultLabel: "Оберіть де відкривати",
					items: {
						_self: "В поточній вкрадці",
						_blank: "В новій вкрадці"
					}
				})
			})
		]
	}),
	model("network-stat-item", null, {
		fieldsAppearance: {
			grid: [
				["stats","caption"]
			]
		},
		fields: [
			string("stats", "Заголовок"),
			string("caption", "Опис"),
		]
	}),
	model("rtb", "Reasons to believe", {
		fields: [
			string("title", "Rtb Block title"),
			array("items", "Елементи статистики", {
				limits: { maxCount: 4 },
				items: useModel("rtb-item")
			})
		]
	}),
	model("stats", "Статистика", {
		fields: [
			array("items", "Елементи статистики", {
				limits: { maxCount: 3 },
				items: useModel("stats-item")
			})
		]
	}),
	model("links", "Item of reasons to believe", {
		// appearanceValue: { size: "large" },
		fields: [
			array("items", "Ссилки", {
				limits: { maxCount: 4 },
				// validate: {
				// 	key: "validateItemsCount",
				// 	config: { min: 1, max: 4 },
				// 	description: "Необхідно обрати щонайменше 1 ссилку, але не більше 4-x"
				// },
				items: useModel("link-item")
			})
		]
	}),
	model("cta", "Call to Action block", {
		fields: [
			string("title", "Заголовок"),
			multilineString("text", "Лід"),
			createLinkItemField()
		]
	}),
	model("news", "Пов'язані новини", {
		fields: [
			string("title", "Заголовок", {
				validate: {
					key: "isRequired",
					config: null,
					description: "Це поле обов\'язкове. Заповніть його будь-ласка."
				}
			}),
			custom("items", "Список новин", "SearchList", {
				value: [],
				limits: {
					maxCount: 3
				},
				validate: {
					key: "validateItemsCount",
					config: { min: 1, max: 3 },
					description: "Необхідно обрати щонайменше 1 новину, але не більше 3-x"
				},
				options: {
					endpoint: "/api/v1/news?search=",
					storeProps: ["data.id", "data.title"],
					previewProp: "title"
				},
			}),
			createLinkItemField()
		]
	}),
	model("network-stations", "Карта", {
		fieldsAppearance: {
			previewKey: "title",
			grid: [
				["title"],
				["statsItems"],
				["fuel", "service"],
				["hideFilters"]
			]
		},
		fields: [
			string("title", "Заголовок"),
			array("statsItems", "Елементи статистики", {
				limits: { maxCount: 2 },
				items: useModel("network-stat-item")
			}),
			custom("fuel", "Пальне", "Filters", {
				options: {
					endpoint: "/api/v1/network-stations/fuels"
				}
			}),
			custom("service", "Сервіси", "Filters", {
				options: {
					endpoint: "/api/v1/network-stations/services"
				}
			}),
			boolean("hideFilters", "Виведення на фільтрів на сайт", {
				value: false,
				options: {
					label: "Приховати фільтри. Користувач не бачитиме фільтри на сайті"
				}
			})
			
		]
	})
	
	
];

module.exports = configModels;

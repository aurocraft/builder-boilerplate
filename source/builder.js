import "@babel/polyfill/noConflict";
import configure from "@aurocraft/builder";
import { externalIcons } from "./main/icons";
import { externalFields } from "./main/fields";
import { externalValidations } from "./main/validations";
import { externalViews } from "./main/views";
import serialize from "./main/utils/serialize"
import deserialize from "./main/utils/deserialize";

async function initBuilder(globalConfig) {
	
	const { render, getState, setState, rootNode } = await configure({
		config: {
			deserialize,
			serialize,
			/**
			 * Run before deserialization
			 * @param data - stored data.
			 */
			beforeEncode: function(data) {
				return data
			},
			/**
			 * Run after store all fields to storage
			 * @param data - serialized data from Storage
			 */
			afterDecode: function (data) {
				return data;
			},
			listenEvents: "*", // is equivalent to ["onInit", "onChange", "onUpdate", "onForceUpdated"]
			arrayItemsCollapsed: true,
			globalConfig,
			editorConfig: "/assets/editor.config.json"
		},
		icons: externalIcons,
		fields: externalFields,
		views: externalViews,
		validations: externalValidations,
		root: "#block-builder-draft-root"
	});
	render(() => {
		console.log("App was rendered");
	});
	
	
	rootNode.addEventListener("onChange", (e) => {
		console.log("onChange", e.detail)
	});
	
	rootNode.addEventListener("onUpdate", (e) => {
		console.log("onUpdate", e.detail)
	});
	
	rootNode.addEventListener("onInit", (e) => {
		console.log("onInit", e.detail)
	});
	
	rootNode.addEventListener("onForceUpdated", (e) => {
		console.log("onForceUpdated", e.detail)
	});
	
	document.addEventListener('builderPopupShown', () => {
		document.body.classList.add("is-frozen-page");
	});
	document.addEventListener('builderPopupHidden', () => {
		document.body.classList.remove("is-frozen-page");
	});
}

// 1. static
const GLOBAL_CONFIG = {
	config: {
		name: "article_form[content]", // name for textarea `store`
	},
	items: [
		{
			id:"item_rtb_lXCIKjP",
			key:"raw",
			fields:{
				title:"",
				appearance:{
					fill:"clear",
					size:"small"
				}
			}
		}
	],
	buttons: [
		{"key":"raw","title":"Ricje","icon":"TextAlignFull"},
		{"key":"image-text","title":"Image Texr","icon":"TextWrap"},
	]
};

// 2. from window
// const GLOBAL_CONFIG = window.__APP_STATE__;

// 3. from server
// fetch("/global/config/endpoint").then(res => res.json()).then(data => initBuilder(data));

(async function () {
	await initBuilder(GLOBAL_CONFIG);
})();


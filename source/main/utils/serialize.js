/**
 * From Editor State to Storage. Example usage in  "../shell-examples/serialize.js"
 * @param { String } modelKey - key of model - `raw, carousel, etc`. In Array-type its key of element model
 * @param { String } fieldKey - key of field - `title, text, etc`
 * @param { any } fieldValue - value of field
 * @return { any }
 */

const serialize = ({ modelKey, fieldKey, fieldValue }) => {
	// feel free and do something cool or just return input `fieldValue`
	return fieldValue;
};

export default serialize;

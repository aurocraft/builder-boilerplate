const deserialize = ({ modelKey, fieldKey, fieldValue }) => {
	/**
	 * Add prop `media` level for every item
	 */
	if(modelKey === "gallery" && fieldKey === "items") {
		return fieldValue.map(item => ({ media: item }));
	}
	
	/**
	 * Return object as one-item array
	 */
	if (fieldKey === "button") {
		return [fieldValue]
	}
	
	/**
	 * Do nothing. Just return
	 */
	return fieldValue
};

export default deserialize;

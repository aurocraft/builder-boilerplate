const serialize = ({ modelKey, fieldKey, fieldValue }) => {
	/**
	 * Extract first `object`
	 */
	if((modelKey === "cta" || modelKey === "news") && fieldKey === "button") {
		return fieldValue[0];
	}
	
	/**
	 * Extract data of prop `media` of every item
	 */
	if(modelKey === "gallery" && fieldKey === "items") {
		return fieldValue.map(item => item["media"])
	}
	
	/**
	 * Do nothing. Just return
	 */
	return fieldValue;
};

export default serialize;

import React from "react";

const QuoteView = ({item}) => {
	
	return (
		<div>
			{ item.fields.text } <br />
			<b>{ item.fields.author }</b>
		</div>
	);
};

export default QuoteView;

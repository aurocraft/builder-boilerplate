import TemplateView from "./TemplateView/TemplateView";
import QuoteView from "./QuoteView/QuoteView";

const externalViews = {
	TemplateView,
	QuoteView,
};

export { externalViews };

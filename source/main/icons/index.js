import { Bug } from "grommet-icons/icons/Bug";

/**
 * Extend default list of icons
 * Use for set icons of block buttons
 * Source: https://icons.grommet.io/
 *
 * !!!Attention
 * In current version (4.2.0) missed auto three shaking. For importing only certain icon use next schema
 * import { ICONNAME } from "grommet-icons/icons/ICONNAME";
 */

const externalIcons = {
	Bug
};

export { externalIcons };

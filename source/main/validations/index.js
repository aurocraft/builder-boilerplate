import { isRequired } from "./isRequired";
import { validateItemsCount } from "./validateItemsCount";
import { isLink } from "./isLink";

const externalValidations = {
	isRequired,
	validateItemsCount,
	isLink
};

export { externalValidations };

const validateItemsCount = (config) => (value) => {
	const { min, max } = config;
	
	return value.length >= min && value.length <= max;
};

export { validateItemsCount };

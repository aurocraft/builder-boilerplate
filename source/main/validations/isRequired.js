const isRequired = (config) => value => {
	return !!value
};

export { isRequired };

import isURL from "validator/lib/isURL";

const isLink = config => value => {
	return isURL(value, { protocols: ["https"] })
};

export { isLink };

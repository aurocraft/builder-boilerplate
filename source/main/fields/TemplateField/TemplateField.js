import React from "react";
import PropTypes from "prop-types";
import { Input, Container } from "./styled";

/**
 * Template field for editor
 * @param value - passed value from Store
 * @param options - options or/and config
 * @param onChange - passed value to Store
 * @return {*}
 * @constructor
 */
const TemplateField = ({ value, options, onChange }) => {
	
	const changeHandler = (e) => {
		onChange(e.target.value);
	};
	
	return (
		<Container>
			<Input type={options.inputType} value={value} onChange={changeHandler} />
		</Container>
	);
	
};

TemplateField.propTypes = {
	options: PropTypes.object,
	value: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
};

TemplateField.defaultProps = {
	value: "",
};

export default TemplateField;

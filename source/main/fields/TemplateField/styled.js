import styled from "styled-components";

const Container = styled.div`
	width: 100%;
	height: auto;
	padding: 20px;
`;
const Input = styled.input`
	width: 240px;
	height: 36px;
	background-color: white;
	border: 1px solid crimson;
`;

export {
	Container, Input
}

import TemplateField from "./TemplateField";

const externalFields = {
	"TemplateKey": TemplateField,
};

export { externalFields };

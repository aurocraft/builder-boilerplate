const path = require("path");

module.exports = {
  entry: {
    builder: "./source/builder.js",
  },
  output: {
    filename: "./js/[name].js",
    path: path.resolve(__dirname + "/assets")
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        use: "babel-loader"
      },
    ],
  },
  devtool: "source-map"
};
